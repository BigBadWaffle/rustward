# rustward

# Debug setup

Set the `RUST_LOG` environment variable to either

 * info
 * debug
 * trace

to see info printed by the log macros.

# Json

Move player JSON example

```json
{
    "Move": [2,4]
}
```

Auth event is just the string auth like so

```json
"Auth"
```

# Rust tests see output

To see the output of rust tests you need to add the nocapture flag like so:

```bash
cargo t -- --nocapture
```

# Running the static server

The static server will serve files from the client directory. In order for the server to find the files you need to run cargo from the `server-runtime` directory:

```bash
cd server-runtime
cargo r
```

The you can browse to http://localhost:3000/index.html to get the first file