const consts = {
	rendering: {
		tileSize: 40,
		tileScale: 5
	}
};

export const rendering = consts.rendering;

export default consts;
