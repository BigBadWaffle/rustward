const fileList = [
	'images/tiles.png',
	'images/mobs.png'
];

let sprites = {};

export const init = async () => {
	return Promise.all(fileList.map(s => {
		return new Promise(res => {
			const sprite = new Image();
			sprites[s] = sprite;
			sprite.onload = res;
			sprite.src = s;
		});
	}));
};

export const getSpriteEntries = () => Object.entries(sprites);
