import * as PIXI from 'pixi.js';

import { init as initResources, getSpriteEntries } from './resources';

import { rendering } from '../config/consts';
const { tileSize } = rendering;

let renderer;
let stage;
let textures = {};
const textureCache = {};

const initTextures = async () => {
	await initResources();

	getSpriteEntries().forEach(([name, sprite]) => {
		textures[name] = new PIXI.BaseTexture(sprite);
		textures[name].scaleMode = PIXI.SCALE_MODES.NEAREST;
	});
};

const getTexture = (textureName, cell) => {
	const baseTexture = textures[textureName];

	const size = 8;

	const cacheEntryName = textureName + '_' + cell;

	let cached = textureCache[cacheEntryName];

	if (!cached) {
		const y = ~~(cell / 8);
		const x = cell - (y * 8);

		cached = new PIXI.Texture(
			baseTexture,
			new PIXI.Rectangle(x * size, y * size, size, size)
		);

		textureCache[textureName] = cached;
	}

	return cached;
};

const buildTile = (x, y, cell) => {
	const sprite = new PIXI.Sprite();

	sprite.width = tileSize;
	sprite.height = tileSize;
	sprite.texture = getTexture('images/tiles.png', cell);
	sprite.x = x;
	sprite.y = y;
	sprite.alpha = 0.3 + (Math.random() * 0.5);

	return sprite;
};

export const addContainer = () => {
	const container = new PIXI.Container();

	stage.addChild(container);

	return container;
};

export const addPlayer = cell => {
	const sprite = new PIXI.Sprite();
	sprite.width = tileSize;
	sprite.height = tileSize;
	sprite.texture = getTexture('images/mobs.png', cell);

	return sprite;
};

export const addTextSprite = text => {
	const textSprite = new PIXI.Text(text, {
		fontFamily: 'bitty',
		fontSize: 14,
		fill: 0xF2F5F5,
		stroke: 0x2d2136,
		strokeThickness: 1,
		align: 'center'
	});

	textSprite.x = 0;
	textSprite.y = tileSize + (tileSize / 4);

	return textSprite;
};

const buildWorld = () => {
	const cellOptions = [0, 1, 3, 4, 8];

	for (let i = 0; i < 24; i++) {
		for (let j = 0; j < 15; j++) {
			const cell = cellOptions[~~(Math.random() * cellOptions.length)];

			const tile = buildTile(i * tileSize, j * tileSize, cell);
			stage.addChild(tile);
		}
	}
};

export const init = async container => {
	await initTextures();

	PIXI.settings.GC_MODE = PIXI.GC_MODES.AUTO;
	PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
	PIXI.settings.SPRITE_MAX_TEXTURES = Math.min(PIXI.settings.SPRITE_MAX_TEXTURES, 16);
	PIXI.settings.RESOLUTION = 1;

	const view = document.getElementById('canvas');
	view.width = 960;
	view.height = 600;

	renderer = new PIXI.Renderer({
		width: 960,
		height: 600,
		backgroundColor: '0x2d2136',
		view
	});

	stage = new PIXI.Container();

	buildWorld();

	renderer.render(stage);

	//container.appendChild(renderer.view);
};

export const redraw = () => {
	if (!renderer)
		return;

	renderer.render(stage);
};
