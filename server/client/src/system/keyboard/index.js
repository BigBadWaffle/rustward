import config from './config';

import { emitEvent } from '../ws';

const keyStates = {};

const keyDown = e => {
	const key = String.fromCharCode(e.which);
	let state;

	if (!keyStates[key])
		state = config.states.KEY_DOWN;
	else if (keyStates[key] === config.states.KEY_DOWN)
		state = config.states.KEY_PRESSED;

	keyStates[key] = state;

	emitEvent({
		event: 'Keyboard',
		msg: {
			key,
			state
		}
	});
};

const keyUp = e => {
	const key = String.fromCharCode(e.which);

	if (!keyStates[key])
		return;

	delete keyStates[key];
	emitEvent({
		event: 'Keyboard',
		msg: {
			key,
			state: config.states.KEY_UP
		}
	});
};

export const init = () => {
	window.addEventListener('keydown', keyDown);
	window.addEventListener('keyup', keyUp);
};
