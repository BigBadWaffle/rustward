const config = {
	states: {
		//For when the key is released
		KEY_UP: 0,
		//For when the key is pressed the first time
		KEY_DOWN: 1,
		//While the key is being held down
		KEY_PRESSED: 2
	}
};

export default config;
