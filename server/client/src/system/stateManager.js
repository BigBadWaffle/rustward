import { subscribeToEvent } from '../system/ws';

const props = {};
const states = {};
const setters = {};

const getState = id => {
	return states[id] ?? {};
};

const setState = (id, state) => {
	setters[id](prev => {
		const newState = Object.assign(
			JSON.parse(JSON.stringify(prev)),
			state
		);

		Object.entries(newState).forEach(([k, v]) => {
			states[id][k] = v;
		});

		return newState;
	});
};

const register = async (id, setter, state) => {
	states[id] = state;
	setters[id] = setter;

	const configModule = await import(
		/* webpackMode: "eager" */
		`../ui/components/${id}/config`
	);

	const _props = {
		config: configModule.default,
		state: states[id],
		setState: setState.bind(null, id)
	};

	_props.wrapFn = (handler, ...rest) => {
		return handler.bind(null, _props, ...rest);
	};

	_props.subscribeToEvent = ({ event, handler }) => {
		subscribeToEvent({
			source: id,
			event,
			handler
		});
	};

	props[id] = _props;

	return _props;
};

export {
	register,
	getState,
	setState
};
