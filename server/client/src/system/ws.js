//Config
import { serverPath } from '../config';

//Internal
let socket;
const eventSubscriptions = [];

const onMessage = ({ data: eventData }) => {
	const parsed = JSON.parse(eventData);

	const [[_event, _msg]] = Object.entries(parsed);

	eventSubscriptions.forEach(({ event, handler }) => {
		if (event !== _event)
			return;

		handler(_msg);
	});
};

export const init = () => {
	socket = new WebSocket(serverPath);

	socket.addEventListener('message', onMessage);
};

//Helper
export const sendPacket = async packet => {
	socket.send(JSON.stringify(packet));
};

export const subscribeToEvent = ({ event, handler }) => {
	eventSubscriptions.push({
		event,
		handler
	});
};

export const emitEvent = ({ event: _event, msg }) => {
	eventSubscriptions.forEach(({ event, handler }) => {
		if (event !== _event)
			return;

		handler(msg);
	});
};
