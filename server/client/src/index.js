//React
import React from 'react';
import ReactDOM from 'react-dom/client';

//UI
import System from './ui/components/system';
import Login from './ui/components/login';

import { init as initWs } from './system/ws';
import { init as initKeyboard } from './system/keyboard';
import { init as initEcs } from './ecs';
import { init as initRenderer } from './renderer';

//Styles
import './styles/index.css';

//Main
const root = ReactDOM.createRoot(document.getElementById('root'));

initWs();
initEcs();
initKeyboard();
initRenderer();

root.render(
	<>
		<System />
		<Login />
	</>
);
