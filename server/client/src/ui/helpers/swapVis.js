//System
import { setState } from '../../system/stateManager';

//Helpers
const swapVis = (toHide, toShow) => {
	setState(toHide, { vis: false });

	setState(toShow, { vis: true });
};

export const swapVisExtra = (toHide, toShow, extraStates) => {
	setState(toHide, { vis: false });

	setState(toShow, {
		vis: true,
		...extraStates
	});
};

export default swapVis;
