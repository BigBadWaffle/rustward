//React
import { useState, useEffect } from 'react';

//System
import { register } from '../../../system/stateManager';

//Events

const onMount = (setter, state) => {
	(async () => {
		const { setState } = await register('system', setter, state);

		window.isMobile = (
			/Mobi|Android/i.test(navigator.userAgent) ||
			(
				navigator.platform === 'MacIntel' &&
				navigator.maxTouchPoints > 1
			)
		);

		if (window.isMobile)
			document.getElementById('root').classList.add('mobile');

		setState({ isMobile });
	})();
};

//Components
const System = () => {
	const [state, setter] = useState({ isMobile: false });

	useEffect(onMount.bind(null, setter, state), []);

	return null;
};

export default System;
