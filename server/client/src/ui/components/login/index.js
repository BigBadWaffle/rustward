//React
import { useState, useEffect } from 'react';

//System
import { register, setState } from '../../../system/stateManager';

//System Helpers
import { sendPacket, subscribeToEvent } from '../../../system/ws';

//Styles
import './styles.css';

//Events
const onLoginResult = (props, result) => {
	if (!result.Failure || !result.failure)
		props.setState({ vis: false });
};

const onMount = (setter, state) => {
	(async () => {
		const { wrapFn } = await register('login', setter, state);

		subscribeToEvent({
			source: 'login',
			event: 'LoginResult',
			handler: wrapFn(onLoginResult)
		});
	})();
};

const onClickLogin = () => {
	const username = document.getElementById('username').value;
	const password = document.getElementById('password').value;

	setState('login', {
		username,
		password
	});

	sendPacket({
		username,
		password
	});
};

//Components
const Login = () => {
	const [state, setter] = useState({ vis: true });

	useEffect(onMount.bind(null, setter, state), []);

	if (!state.vis)
		return;

	return (
		<div className='cpnLogin centered'>
			<div className='title'>
				Login
			</div>
			<div className='bottom'>
				<input id='username' placeholder='username...' />
				<input
					id='password'
					type='password'
					placeholder='password...'
				/>
			</div>
			<div className='toolbar'>
				<div className='btn right' onClick={onClickLogin}>
					Login
				</div>
			</div>
		</div>
	);
};

export default Login;
