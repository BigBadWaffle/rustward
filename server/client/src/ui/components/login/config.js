const config = {
	events: {
		LoginResult: {
			success: 'Success',
			failure: 'Failure'
		}
	}
};

export default config;
