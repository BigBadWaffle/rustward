import { registerComponent } from './index';

import Transform from './components/transform';
import Renderable from './components/renderable';
import Sprite from './components/sprite';
import TextSprite from './components/textSprite';
import ControllerKeyboard from './components/controllerKeyboard';

const ComponentLibrary = {
	transform: Transform,
	renderable: Renderable,
	sprite: Sprite,
	textSprite: TextSprite,
	controllerKeyboard: ControllerKeyboard
};

export const init = () => {
	Object.values(ComponentLibrary).forEach(T => registerComponent(T));
};

export const getComponentOfType = type => {
	return ComponentLibrary[type];
};
