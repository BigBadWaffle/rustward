import { Engine } from 'geotic';

import { subscribeToEvent } from '../system/ws';
import { getComponentOfType, init as initComponentLibrary } from './componentLibrary';
import { redraw } from '../renderer';

const messages = [];

let engine;
let world;

let qControllerKeyboard;

/* eslint-disable-next-line max-lines-per-function */
const syncEntities = ({ entities }) => {
	const syncs = Object
		.keys(entities)
		.map(k => {
			return {
				id: entities[k].components['engine::player::ID'],
				components: [
					{
						type: 'transform',
						position: {
							x: entities[k].components['engine::IWDTransform'].position.x,
							y: entities[k].components['engine::IWDTransform'].position.y
						}
					},
					{
						type: 'textSprite',
						text: entities[k].components['bevy_core::name::Name'].name
					},
					{ type: 'renderable' },
					{
						type: 'sprite',
						cell: 0
					}
				]
			};
		});

	syncs[syncs.length - 1].components.push({ type: 'controllerKeyboard' });

	syncs.forEach(({ id, components }) => {
		let entity = world.getEntity(id);
		if (!entity) {
			world.createEntity(id);
			entity = world.getEntity(id);
		}

		components.forEach(({ type, ...rest }) => {
			const Type = getComponentOfType(type);

			if (!entity.has(Type))
				entity.add(Type, rest);
		});
	});
};

const keyboard = msg => {
	qControllerKeyboard.get().forEach(e => e.controllerKeyboard.handleInput(msg));
};

const move = ({ entities }) => {
	Object
		.keys(entities)
		.map(k => {
			return {
				id: entities[k].components['engine::player::ID'],
				transform: {
					x: entities[k].components['engine::IWDTransform'].position.x,
					y: entities[k].components['engine::IWDTransform'].position.y
				}
			};
		}).forEach(({ id, transform: { x, y } }) => {
			const entity = world.getEntity(id);
			if (!entity)
				return;

			entity.fireEvent('move', {
				x,
				y
			});
		});
};

const processMessages = () => {
	messages.forEach(({ type, msg }) => {
		if (type === 'syncEntities')
			syncEntities(msg);
		else if (type === 'keyboard')
			keyboard(msg);
		else if (type === 'move')
			move(msg);
	});

	messages.length = 0;
};

const tick = () => {
	processMessages();

	qControllerKeyboard.get().forEach(e => e.fireEvent('tick'));

	setTimeout(tick, 350);
};

const render = () => {
	redraw();

	requestAnimationFrame(render);
};

const queueMessage = (type, msg) => {
	messages.push({
		type,
		msg
	});
};

const createOnAddedSystem = (componentList, handler) => {
	world
		.createQuery({
			all: componentList.map(c => getComponentOfType(c)),
			immutableResults: false
		})
		.onEntityAdded(handler);
};

const createQuery = componentList => {
	return world
		.createQuery({
			all: componentList.map(c => getComponentOfType(c)),
			immutableResults: false
		});
};

export const init = () => {
	engine = new Engine();
	world = engine.createWorld();

	subscribeToEvent({
		event: 'SyncAll',
		handler: queueMessage.bind(null, 'syncEntities')
	});

	subscribeToEvent({
		event: 'Keyboard',
		handler: queueMessage.bind(null, 'keyboard')
	});

	subscribeToEvent({
		event: 'Delta',
		handler: queueMessage.bind(null, 'move')
	});

	initComponentLibrary();

	qControllerKeyboard = createQuery(['controllerKeyboard']);

	createOnAddedSystem(
		['renderable', 'transform'],
		e => e.renderable.updateContainer(e.transform.position)
	);

	createOnAddedSystem(
		['renderable', 'sprite'],
		e => e.sprite.addSpriteToContainer(e.renderable.container)
	);

	createOnAddedSystem(
		['renderable', 'textSprite'],
		e => e.textSprite.addSpriteToContainer(e.renderable.container)
	);

	tick();
	render();
};

export const registerComponent = Type => {
	engine.registerComponent(Type);
};
