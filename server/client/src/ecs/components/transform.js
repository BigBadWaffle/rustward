import { Component } from 'geotic';

export default class Transform extends Component {
	static properties = {
		position: {
			x: null,
			y: null
		}
	};

	onMove ({ x, y }) {
		this.position.x = x;
		this.position.y = y;
	}
}
