import { Component } from 'geotic';

import { rendering } from '../../config/consts';
const { tileSize } = rendering;

import { addContainer } from '../../renderer';

export default class Renderable extends Component {
	static properties = { container: null };

	onAttached () {
		this.container = addContainer();
	}

	addSprite (sprite) {
		this.container.addChild(sprite);
	}

	updateContainer ({ x, y }) {
		this.container.x = x * tileSize;
		this.container.y = y * tileSize;
	}

	onMove ({ data }) {
		this.updateContainer(data);
	}
}
