import { Component } from 'geotic';

import { addPlayer } from '../../renderer';

export default class Sprite extends Component {
	static properties = {
		cell: null,
		sprite: null
	};

	onAttached () {
		this.sprite = addPlayer(this.cell);
	}

	addSpriteToContainer (container) {
		container.addChild(this.sprite);
	}
}
