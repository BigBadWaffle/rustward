import { Component } from 'geotic';

import { addTextSprite } from '../../renderer';

export default class TextSprite extends Component {
	static properties = {
		text: null,
		sprite: null
	};

	onAttached () {
		this.sprite = addTextSprite(this.text);
	}

	addSpriteToContainer (container) {
		container.addChild(this.sprite);
	}
}
