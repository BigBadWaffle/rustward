import { Component } from 'geotic';

import keyboardConfig from '../../system/keyboard/config';

import { sendPacket } from '../../system/ws';

const keyMappingToDelta = {
	Q: [-1, -1],
	W: [0, -1],
	E: [1, -1],
	A: [-1, 0],
	S: [0, 1],
	D: [1, 0],
	Z: [-1, 1],
	X: [0, 1],
	C: [1, 1]
};

export default class ControllerKeyboard extends Component {
	static properties = { moveDelta: null };

	handleInput ({ key, state }) {
		if (state === keyboardConfig.states.KEY_UP) {
			this.moveDelta = null;

			return;
		}

		if (state !== keyboardConfig.states.KEY_DOWN)
			return;

		const delta = keyMappingToDelta[key];
		if (!delta)
			return;

		this.moveDelta = delta;
	}

	onTick () {
		const { moveDelta } = this;

		if (!moveDelta)
			return;

		sendPacket({ Move: moveDelta });
	}
}
