use engine::{actions::ActionData, auth::LoginData};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum NetworkPacket {
    Login(LoginData),
    Action(ActionData),
}
