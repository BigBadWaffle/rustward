use database::Database;
use engine::Engine;
// use events::{write_login_events, write_move_events, write_respawn_events, GameEvent};

use std::{net::SocketAddr, time::Duration};

use bridge::EngineServerBridge;

mod bridge;
mod clients;
mod http;
mod message;

const LISTEN_PORT: u16 = 8080;
pub const NETWORK_TICK: Duration = Duration::from_millis(50);
fn main() {
    tracing_subscriber::fmt::init();
    let addr: SocketAddr = format!("127.0.0.1:{LISTEN_PORT}").parse().unwrap();
    let bridge = EngineServerBridge::new(addr);
    http::spawn_static_server(bridge.runtime());
    let db = Database::new();
    Engine::start(bridge, db);
}
