use bevy::log::{debug, info};
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Response, Server};
use std::convert::Infallible;

use std::fs;
use std::path::Path;
use tokio::runtime::Runtime;

const ROOT: &str = "./client";
const SERVER_PORT: u16 = 3000;
async fn handle(req: Request<Body>) -> Result<Response<Body>, Infallible> {
    let root = Path::new(ROOT);
    debug!("root: {:?}", fs::canonicalize(root));
    let result = hyper_staticfile::resolve(&root, &req).await.unwrap();
    debug!("file result: {:?}", result);
    // Then, build a response based on the result.
    // The `ResponseBuilder` is typically a short-lived, per-request instance.
    Ok(hyper_staticfile::ResponseBuilder::new()
        .request(&req)
        .build(result)
        .unwrap())
}

pub fn spawn_static_server(rt: &Runtime) {
    debug!(
        "Files around server exe: {:?}",
        Path::new("./")
            .read_dir()
            .unwrap()
            .into_iter()
            .for_each(|x| debug!("{:?}", x))
    );
    rt.spawn(async move {
        // Construct our SocketAddr to listen on...
        let addr = format!("127.0.0.1:{SERVER_PORT}").parse().unwrap();
        // And a MakeService to handle each connection...
        let make_service =
            make_service_fn(|_conn| async { Ok::<_, Infallible>(service_fn(handle)) });

        // Then bind and serve...
        let server = Server::bind(&addr).serve(make_service);

        info!("Static server bound. Listening on {addr}");
        // And run forever...
        if let Err(e) = server.await {
            eprintln!("server error: {}", e);
        }
    });
}
