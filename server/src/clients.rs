use crate::bridge::SocketWriter;
use bevy::ecs::entity::Entity;
use tokio::sync::Mutex;

use tokio::sync::RwLock;

use std::collections::HashMap;

use std::sync::atomic::AtomicU64;
use std::sync::atomic::Ordering;
use std::sync::{Arc, RwLock as SyncRwLock};

type ClientSocketWriter = Arc<Mutex<SocketWriter>>;
type ClientLookup = &'static RwLock<HashMap<u64, ClientSocketWriter>>;
type EntityLookup = &'static SyncRwLock<HashMap<u64, Entity>>;

static CURRENT_ID: AtomicU64 = AtomicU64::new(0);

#[derive(Debug, Clone, Copy)]
pub struct Clients {
    pub lookup: ClientLookup,
    pub entity_lookup: EntityLookup,
}

impl Clients {
    pub fn new() -> Self {
        Self {
            lookup: Box::leak(Box::new(RwLock::new(HashMap::with_capacity(100)))),
            entity_lookup: Box::leak(Box::new(SyncRwLock::new(HashMap::with_capacity(100)))),
        }
    }

    pub async fn add(&self, writer: SocketWriter) -> u64 {
        let wr = Arc::new(Mutex::new(writer));
        let wr2 = Arc::clone(&wr);
        let id = CURRENT_ID.fetch_add(1, Ordering::Relaxed);
        self.lookup.write().await.insert(id, wr2);
        id
    }

    pub fn insert_entity(&self, id: u64, entity: Entity) {
        self.entity_lookup.write().unwrap().insert(id, entity);
    }

    pub fn remove_entity(&self, id: &u64) -> Option<Entity> {
        self.entity_lookup.write().unwrap().remove(id)
    }
}
