use crate::clients::Clients;
use crate::message::NetworkPacket;
use crate::NETWORK_TICK;
use bevy::ecs::entity::Entity;
use bevy::prelude::Resource;
use crossbeam_channel::TryRecvError;
use crossbeam_channel::{Receiver, Sender};
use engine::actions::{ActionData, NetworkAction};
use engine::auth::Login;
use engine::bridge;
use engine::message::Response;
use futures_util::SinkExt;

use futures_util::stream::SplitSink;
use futures_util::stream::SplitStream;

use bevy::log::{debug, error, info, trace, warn};
use tokio::net::TcpStream;

use tokio_tungstenite::WebSocketStream;

use std::net::SocketAddr;

use tokio::sync::Mutex;

use futures_util::StreamExt;

use tokio::net::TcpListener;

use tokio::runtime::Runtime;
use tungstenite::Message;

pub type EngineLoginReceiver = Receiver<Login>;
pub type EngineLoginSender = Sender<Login>;
pub type EngineActionReceiver = Receiver<NetworkAction>;
pub type EngineActionSender = Sender<NetworkAction>;

pub type SocketWriter = SplitSink<WebSocketStream<TcpStream>, Message>;
pub type SocketReader = SplitStream<WebSocketStream<TcpStream>>;

type Responses = &'static Mutex<Vec<(u64, Receiver<Response>)>>;

#[derive(Debug, Clone, Resource, Copy)]
pub struct EngineServerBridge {
    clients: Clients,
    receivers: &'static Receivers,
    senders: &'static Senders,
    runtime: &'static Runtime,
    engine_responses: Responses,
}
impl EngineServerBridge {
    pub fn new(ws_listen_address: SocketAddr) -> Self {
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_io()
            .enable_time()
            .enable_io()
            .build()
            .unwrap();

        let (senders, receivers) = GameChannels::create();
        let clients = Clients::new();

        let runtime = Box::leak(Box::new(rt));
        let engine_responses = Box::leak(Box::new(Mutex::new(Vec::with_capacity(100))));
        let ret = Self {
            clients,
            receivers: Box::leak(Box::new(receivers)),
            senders: Box::leak(Box::new(senders)),
            runtime,
            engine_responses,
        };
        runtime.spawn(async move {
            let listener = TcpListener::bind(&ws_listen_address)
                .await
                .expect("Can't listen");
            info!("Listening on: {}", ws_listen_address);
            WsListener::new(ret).run(listener).await;
        });
        let res_lis = ret;
        runtime.spawn(async move {
            res_lis.spawn_response_listener().await;
        });
        ret
    }
    async fn remove_disconnected(&self, connections_to_remove: &mut Vec<u64>) {
        while let Some(id) = connections_to_remove.pop() {
            if let Some(ent) = self.clients.remove_entity(&id) {
                trace!("Sending despawn action for entity {ent:?}");
                if let Err(e) = self.senders.action.send(NetworkAction {
                    entity_id: ent,
                    data: ActionData::Despawn,
                }) {
                    error!("{e}. Could not send despawn to game engine for entity {ent:?}.");
                }
            }
            trace!("Removing client WebSocket writer for connection id {id}");
            self.clients.lookup.write().await.remove(&id);
        }
    }

    async fn spawn_response_listener(&self) {
        let mut to_return = Vec::with_capacity(100);
        let mut connections_to_remove = Vec::with_capacity(100);
        loop {
            let mut lock = self.engine_responses.lock().await;
            while let Some((id, recv)) = lock.pop() {
                match recv.try_recv() {
                    Ok(res) => {
                        trace!("Server got game response {res:?} on connection {id}");
                        if let Some(writer) = self.clients.lookup.read().await.get(&id) {
                            let msg = Message::Text(serde_json::to_string(&res).unwrap());
                            match writer.lock().await.send(msg).await {
                                Ok(_) => {}
                                Err(e) => {
                                    trace!("{e}. Could not send response to connection id {id}");
                                    connections_to_remove.push(id);
                                    continue;
                                }
                            }
                        }
                        to_return.push((id, recv));
                    }
                    Err(e) => match e {
                        TryRecvError::Empty => {
                            to_return.push((id, recv));
                            tokio::time::sleep(NETWORK_TICK).await;
                            continue;
                        }
                        TryRecvError::Disconnected => {
                            connections_to_remove.push(id);
                        }
                    },
                }
            }
            lock.append(&mut to_return);
            if !connections_to_remove.is_empty() {
                self.remove_disconnected(&mut connections_to_remove).await;
            }
            // Important drop. If we don't do this then nothing else can ever access the responses.
            drop(lock);
            tokio::time::sleep(NETWORK_TICK).await;
        }
    }

    pub fn runtime(&self) -> &Runtime {
        self.runtime
    }
}
struct GameChannels;
impl GameChannels {
    fn create() -> (Senders, Receivers) {
        let (loginsend, loginrec) = crossbeam_channel::unbounded();
        let (actionsend, actionrec) = crossbeam_channel::unbounded();
        (
            Senders {
                login: loginsend,
                action: actionsend,
            },
            Receivers {
                login: loginrec,
                action: actionrec,
            },
        )
    }
}
#[derive(Debug, Clone)]
struct Senders {
    login: EngineLoginSender,
    action: EngineActionSender,
}

#[derive(Debug, Clone)]
struct Receivers {
    login: EngineLoginReceiver,
    action: EngineActionReceiver,
}

struct WsListener {
    server: EngineServerBridge,
}

impl WsListener {
    fn new(server: EngineServerBridge) -> Self {
        Self { server }
    }
    async fn run(&self, listener: TcpListener) {
        while let Ok((stream, peer)) = listener.accept().await {
            info!("Peer address: {}", peer);
            WsConnection::new(stream, peer, self.server).spawn_handler();
        }
    }
}

struct WsConnection {
    stream: TcpStream,
    peer: SocketAddr,
    server: EngineServerBridge,
}
impl WsConnection {
    fn new(stream: TcpStream, peer: SocketAddr, server: EngineServerBridge) -> Self {
        Self {
            stream,
            peer,
            server,
        }
    }

    fn spawn_handler(self) {
        let WsConnection {
            stream,
            peer,
            server,
        } = self;
        tokio::spawn(async move {
            match tokio_tungstenite::accept_async(stream).await {
                Ok(ws_stream) => {
                    trace!("New WebSocket connection: {peer}");
                    let (write, read) = ws_stream.split();
                    let id = server.clients.add(write).await;
                    trace!("Connection id: {id} for peer {peer}");
                    let reader = WsReader {
                        reader: read,
                        peer,
                        id,
                        server,
                    };
                    reader.spawn_read_loop();
                }
                Err(e) => {
                    debug!("Failed to accept connection from peer {peer}: {e}");
                }
            }
        });
    }
}

impl bridge::Network for EngineServerBridge {
    fn try_receive_login(&self) -> Option<Login> {
        match self.receivers.login.try_recv() {
            Ok(msg) => Some(msg),
            Err(e) => {
                match e {
                    TryRecvError::Empty => None,
                    TryRecvError::Disconnected => {
                        panic!("Engine login receiver Disconnected. Cannot fetch messages for game engine")
                    }
                }
            }
        }
    }
    fn register_entity_on_connection(
        &self,
        ent: Entity,
        connection_id: u64,
        response_receiver: Receiver<Response>,
    ) {
        let server = *self;
        // Not sure if this will cause issues. Might be an issue if alot of players are spawned
        self.runtime().spawn(async move {
            trace!("Inserting response receiver for connection {connection_id}");
            let mut lock = server.engine_responses.lock().await;
            lock.push((connection_id, response_receiver));
            trace!("Responses after insert: {lock:?}");
        });
        self.clients.insert_entity(connection_id, ent);
    }
    fn try_receive_action(&self) -> Option<NetworkAction> {
        match self.receivers.action.try_recv() {
            Ok(msg) => Some(msg),
            Err(e) => match e {
                TryRecvError::Empty => None,
                TryRecvError::Disconnected => {
                    panic!("Engine action receiver Disconnected. Cannot fetch messages for game engine")
                }
            },
        }
    }
}

struct WsReader {
    reader: SocketReader,
    peer: SocketAddr,
    id: u64,
    server: EngineServerBridge,
}
impl WsReader {
    // fn new()
    fn spawn_read_loop(mut self) {
        tokio::spawn(async move {
            while let Some(ws_msg) = self.reader.next().await {
                trace!("Got ws message {:?}", ws_msg);
                let ws_msg = ws_msg.unwrap();
                if ws_msg.is_text() || ws_msg.is_binary() {
                    if let Message::Text(msg) = ws_msg {
                        match serde_json::from_str(&msg) {
                            Ok(NetworkPacket::Login(login_data)) => {
                                trace!("Login received. Sending to engine");
                                let lgn = Login {
                                    server_connection_id: self.id,
                                    data: login_data,
                                };
                                self.server
                                    .senders
                                    .login
                                    .try_send(lgn)
                                    .expect("Login channel to be open and not full")
                            }
                            Ok(NetworkPacket::Action(action)) => {
                                trace!("Action received. Sending to engine");
                                self.send_action(action);
                            }
                            Err(e) => {
                                warn!(
                                    "{e}. Could not deserialize packet {msg} from peer {}",
                                    self.peer
                                );
                            }
                        }
                    } else {
                        error!(
                            "{ws_msg} was in an unsupported format. From peer: {}",
                            self.peer
                        );
                    }
                } else if ws_msg.is_close() {
                    trace!("Connection id {} closed.", self.id);
                    let mut rem = vec![self.id];
                    self.server.remove_disconnected(&mut rem).await;
                    break;
                }
            }
        });
    }

    fn send_action(&self, action: ActionData) {
        if let Some(ent) = self
            .server
            .clients
            .entity_lookup
            .read()
            .unwrap()
            .get(&self.id)
        {
            let act = NetworkAction {
                entity_id: *ent,
                data: action,
            };
            self.server
                .senders
                .action
                .try_send(act)
                .expect("Action channel to be open and not full");
        }
    }
}
