* Not every move currently works
* Sync events (not full sync, delta sync) should also come through as a big SyncDelta event (instead of us getting transform changes)
* We need a DB that supports storing JSON objets

* DB Tables
    login: two fields (username, hashedPwd)
      username: 'shaun',
      password: 'bcrypt hashed pwd'
    characterList: two fields (username, characters: json array)
    ```json
      username: 'shaun',
      characters: [
        {
          name: 'waffle',
          info: {
            level: 10,
            spirit: 'bear'
          }
        },
        {
          name: 'cthera',
          info: {
            level: 20,
            spirit: 'owl'
          }
        }
      ]
    character: two fields (character name, character: json object)
      character name: waffle
      character: {
        this object is the SyncFull result of serializing the character
      }
    ```

* Pub/Priv exmaple
```
selfFull
- transform *
- name
- inventory
    -items
    -gold
- equipment
    -everything I'm wearing



inRangeFull (otherFull)
selfFull
- transform *
- name
- equipment
    -everything I'm wearing
```


Remember:
  SyncAll: Serialize a character with all public fields
  SyncDelta: Serialize a character with all changed fields
  SyncFull: Serialize a character with ALL fields including private (this is for saving and some other cases)

* We need to be able to register an accounrt
* We need to be able to create a character
* We need to be able to see the list of characters
* We need to be able to select a character and spawn it
* Despawn when losing connection

