use crate::{
    actions::{ActionDataType, NetworkAction},
    auth::{Login, LoginData, LoginResult},
    bridge::{Database, Network},
    message::Response,
    player::ID,
    Connection, Player, PlayerBundle,
};
use bevy::prelude::*;

use crate::events::Despawned;

pub fn spawn<N: Network, D: Database>(
    mut commands: Commands,
    mut reader: EventReader<Login>,
    network_bridge: ResMut<N>,
    database_bridge: ResMut<D>,
) {
    for login in reader.iter() {
        let Login {
            data,
            server_connection_id,
        } = &login;
        let LoginData { username, password } = &data;
        trace!("Spawning player for user {username}, connection_id {server_connection_id}");
        let (connection, receiver) = crossbeam_channel::unbounded();
        match database_bridge.authenticate(username, password) {
            Ok(auth) => {
                let mut player = commands.spawn(PlayerBundle::default());
                player
                    .insert(Name::new(username.to_string()))
                    .insert(Connection::new(connection.clone()));
                let ent_id = player.id();
                player.insert(ID(ent_id.to_bits()));

                debug!("Entity id {ent_id:?} for user {username}, connection_id {server_connection_id}");
                match database_bridge.link_player_to_database(auth, ent_id) {
                    Ok(user) => {
                        network_bridge.register_entity_on_connection(
                            ent_id,
                            *server_connection_id,
                            receiver,
                        );
                        let characters = database_bridge.get_character_list(user);
                        player.insert(Player {
                            username: username.to_string(),
                            characters: characters.clone(),
                        });
                        connection
                            .send(Response::LoginResult(LoginResult::Success {
                                characters: characters.into_iter().map(|x| x.to_string()).collect(),
                            }))
                            .unwrap();
                    }
                    Err(_) => {
                        error!("Could not link user {username} to entity id {ent_id:?}");
                        player.despawn_recursive();
                    }
                }
            }
            Err(_) => {
                debug!("Authentication failed for user {username}");
            }
        }
    }
}

pub fn despawn_disonnected(
    mut commands: Commands,
    mut reader: EventReader<NetworkAction>,
    mut writer: EventWriter<Despawned>,
) {
    for action in reader.iter() {
        if action.data == ActionDataType::Despawn {
            trace!("Despawning entity {:?}", action.entity_id);
            commands.entity(action.entity_id).despawn_recursive();
            writer.send(Despawned(action.entity_id));
        }
    }
}
