use bevy::prelude::*;

use crate::{
    actions::{Action, SpawnCharacter},
    bridge::Database,
    character::{Character, CharacterBundle},
    player::Player,
};

pub fn spawn_character_for_player<D: Database>(
    mut reader: EventReader<Action<SpawnCharacter>>,
    players: Query<&Player, Without<Character>>,
    mut commands: Commands,
    database: Res<D>,
) {
    for Action {
        entity_id,
        data: SpawnCharacter(name),
    } in reader.iter()
    {
        if let Ok(player) = players.get(*entity_id) {
            if !player.characters.iter().any(|x| x.as_str() == name) {
                error!(
                    "Could not spawn character {} for player {}",
                    name,
                    player.username.as_str()
                );
                continue;
            }
            let inventory = database.get_character_inventory(name).unwrap_or_default();
            commands
                .entity(*entity_id)
                .commands()
                .spawn(CharacterBundle::default())
                .insert(Name::new(name.to_string()))
                .insert(inventory);
        }
    }
}
