use std::ops::{Deref, DerefMut};

use bevy::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Component, Reflect, Serialize, Clone, Default)]
#[reflect_value]
pub struct Inventory {
    items: Vec<InventoryItem>,
}

impl Deref for Inventory {
    type Target = Vec<InventoryItem>;
    fn deref(&self) -> &Self::Target {
        &self.items
    }
}

impl DerefMut for Inventory {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.items
    }
}

#[derive(Debug, Clone, Copy, Default, Serialize, Reflect, Deserialize)]
pub enum InventoryLevel {
    #[default]
    One = 1,
}

#[derive(Debug, Clone, Reflect, Serialize, Deserialize)]
pub struct InventoryItem {
    ability: bool,
    eq: bool,
    level: InventoryLevel,
    name: String,
    no_salvage: bool,
    quality: usize,
    rune_slot: usize,
    spell: Spell,
    sprite: Sprite,
    worth: usize,
}
#[derive(Debug, Clone, Reflect, Serialize, Deserialize)]
pub enum SpellType {
    Charge,
}
#[derive(Debug, Clone, Reflect, Serialize, Deserialize)]
pub struct Spell {
    name: String,
    rolls: Damage,
    spell_type: SpellType,
    values: Damage,
}

#[derive(Debug, Clone, Reflect, Serialize, Deserialize)]
pub struct Sprite(UVec2, UVec2);

#[derive(Debug, Component, Clone, Serialize, Default, Reflect, Deserialize)]
#[reflect_value(Component, Serialize)]
pub struct Damage {
    amount: f64,
    stun_duration: f64,
}
