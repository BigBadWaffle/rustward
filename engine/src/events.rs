use bevy::prelude::*;

#[derive(Debug, Clone, Copy)]
pub struct HasSeen {
    pub observer_id: Entity,
    pub observed_id: Entity,
}

#[derive(Debug)]
pub struct Despawned(pub Entity);
#[derive(Debug)]
pub struct Delta(pub Entity);
#[derive(Debug)]
pub struct FullSyncRequired;
