use std::sync::Arc;

use bevy::ecs::entity::Entity;
use serde::Serialize;
use serde_json::Value;

use crate::{auth::LoginResult, IWDTransform};

#[derive(Debug, Serialize, Clone)]
pub enum Response {
    LoginResult(LoginResult),
    Transform(TransformUpdate),
    Name(String),
    Despawn(u64),
    SyncAll(Arc<Value>),
    Delta(Arc<Value>),
}
#[derive(Debug, Serialize, Clone, Copy)]
pub struct TransformUpdate(pub u64, pub IWDTransform);

impl From<(Entity, IWDTransform)> for TransformUpdate {
    fn from(t: (Entity, IWDTransform)) -> Self {
        Self(t.0.to_bits(), t.1)
    }
}
