use crate::{
    actions::{Action, Move},
    character::Character,
    events::{Despawned, FullSyncRequired, HasSeen},
    IWDTransform, Player,
};

use bevy::prelude::*;

pub fn require_full_sync_on_add(
    added: Query<Entity, Added<Character>>,
    mut writer: EventWriter<FullSyncRequired>,
) {
    for _ in added.iter() {
        writer.send(FullSyncRequired);
    }
}

pub fn require_full_sync_on_remove(
    mut removed: EventReader<Despawned>,
    mut writer: EventWriter<FullSyncRequired>,
) {
    for _ in removed.iter() {
        writer.send(FullSyncRequired);
    }
}

pub fn add_unseen_entities(
    characters: Query<(Entity, &Character)>,
    all_entities: Query<Entity>,
    mut player_updates: EventWriter<HasSeen>,
) {
    let iter = all_entities.iter().flat_map(|ent| {
        characters
            .iter()
            //TODO: Populate seen_ids,
            .filter(move |(_, character)| !character.in_view.contains(&ent))
            .map(|x| x.0)
            .map(move |x| HasSeen {
                observer_id: x,
                observed_id: ent,
            })
    });
    player_updates.send_batch(iter);
}

pub fn update_in_view(mut characters: Query<&mut Character>, mut updates: EventReader<HasSeen>) {
    for update in updates.iter() {
        if let Ok(mut character) = characters.get_mut(update.observer_id) {
            character.in_view.insert(update.observed_id);
        }
    }
}

pub fn remove_despawned_from_seen(
    mut reader: EventReader<Despawned>,
    mut characters: Query<&mut Character>,
) {
    for Despawned(ent) in reader.iter() {
        for mut character in characters.iter_mut().filter(|x| x.in_view.contains(ent)) {
            character.in_view.remove(ent);
        }
    }
}

pub fn move_player(
    mut query: Query<(Entity, &mut IWDTransform), With<Player>>,
    mut reader: EventReader<Action<Move>>,
) {
    for action in reader.iter() {
        if let Some((_, mut current)) = query.iter_mut().find(|x| &x.0 == action) {
            let how_much = action.data;
            trace!("Moving by {:?}", how_much);
            trace!("IWDTransform before {:?}", current);
            current.position += how_much.delta();
            trace!("IWDTransform after {:?}", current);
        }
    }
}
