use crate::{actions::*, auth::Login, bridge::Network};
use bevy::prelude::*;
pub fn write_client_login<B>(bridge: Res<B>, mut writer: EventWriter<Login>)
where
    B: Network,
{
    // TODO: I think this should probably loop until empty, cause otherwise we will only read 1 login every tick
    //       Might be better to make this a startup system?
    if let Some(client_action) = bridge.try_receive_login() {
        trace!("Writing login event");
        writer.send(client_action);
    }
}
pub fn write_client_move<B>(
    bridge: Res<B>,
    mut writer: EventWriter<Action<Move>>,
    mut unused_writer: EventWriter<NetworkAction>,
) where
    B: Network,
{
    if let Some(action) = bridge.try_receive_action() {
        if action.data == ActionDataType::Move {
            let ActionData::Move(m) = action.data else {
                unreachable!("Filter should have caught this");
            };
            let a = Action::new(action.entity_id, m);
            writer.send(a);
        } else {
            unused_writer.send(action)
        }
    }
}

pub fn read_unused_move(
    mut writer: EventWriter<Action<Move>>,
    mut reader: EventReader<NetworkAction>,
) {
    for action in reader.iter() {
        if action.data == ActionDataType::Move {
            let ActionData::Move(m) = action.data else {
                unreachable!("Filter should have caught this");
            };
            let a = Action::new(action.entity_id, m);
            writer.send(a);
        }
    }
}

pub fn write_client_respawn<B>(
    bridge: Res<B>,
    mut writer: EventWriter<Action<Respawn>>,
    mut unused_writer: EventWriter<NetworkAction>,
) where
    B: Network,
{
    if let Some(action) = bridge.try_receive_action() {
        if action.data == ActionDataType::Respawn {
            let ActionData::Respawn(m) = action.data else {
                unreachable!("Filter should have caught this");
            };
            let a = Action::new(action.entity_id, m);
            writer.send(a);
        } else {
            unused_writer.send(action)
        }
    }
}

pub fn read_unused_respawn(
    mut writer: EventWriter<Action<Respawn>>,
    mut reader: EventReader<NetworkAction>,
) {
    for action in reader.iter() {
        if action.data == ActionDataType::Respawn {
            let ActionData::Respawn(m) = action.data else {
                unreachable!("Filter should have caught this");
            };
            let a = Action::new(action.entity_id, m);
            writer.send(a);
        }
    }
}

pub fn write_client_spawn_character<B>(
    bridge: Res<B>,
    mut writer: EventWriter<Action<SpawnCharacter>>,
    mut unused_writer: EventWriter<NetworkAction>,
) where
    B: Network,
{
    if let Some(action) = bridge.try_receive_action() {
        if action.data == ActionDataType::SpawnCharacter {
            let ActionData::SpawnCharacter(m) = action.data else {
                unreachable!("Filter should have caught this");
            };
            let a = Action::new(action.entity_id, m);
            writer.send(a);
        } else {
            unused_writer.send(action)
        }
    }
}

pub fn read_unused_spawn_character(
    mut writer: EventWriter<Action<SpawnCharacter>>,
    mut reader: EventReader<NetworkAction>,
) {
    for action in reader.iter() {
        if action.data == ActionDataType::SpawnCharacter {
            let ActionData::SpawnCharacter(ref m) = action.data else {
                unreachable!("Filter should have caught this");
            };
            let a = Action::new(action.entity_id, m.clone());
            writer.send(a);
        }
    }
}
