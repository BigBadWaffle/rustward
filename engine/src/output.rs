use crate::IWDTransform;

use bevy::prelude::Entity;
use std::collections::HashMap;

pub type AllTransform = HashMap<Entity, IWDTransform>;
