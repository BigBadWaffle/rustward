use std::{borrow::Cow, time::Instant};

use crate::{
    auth::Login,
    bridge::{Database, Network},
    events::*,
    plugins, systems, IWDTransform, GAME_TICK,
};
use bevy::{
    ecs::schedule::SystemStage,
    log::info,
    prelude::{App, IVec2, Name, StageLabel, UVec2},
};
pub struct Engine;
impl Engine {
    pub fn start<N: Network, D: Database>(network: N, database: D) {
        App::new()
            .insert_resource(network)
            .insert_resource(database)
            .register_type::<IVec2>()
            .register_type::<UVec2>()
            .register_type::<Cow<str>>()
            .register_type::<IWDTransform>()
            .register_type::<Name>()
            .add_event::<Login>()
            .add_event::<Despawned>()
            .add_event::<HasSeen>()
            .add_event::<Delta>()
            .add_event::<FullSyncRequired>()
            .add_stage(Stages::Global, SystemStage::parallel())
            .add_plugin(plugins::AllActions::new(network))
            .add_stage_after(Stages::Global, Stages::First, SystemStage::parallel())
            .add_system_to_stage(Stages::First, systems::regen_hp)
            .add_stage_after(Stages::First, Stages::Second, SystemStage::parallel())
            .add_plugin(plugins::PlayerPlugin::new(network, database))
            .add_plugin(plugins::InventoryPlugin::new(network))
            .add_plugin(plugins::CharacterPlugin::new(network, database))
            .add_system_to_stage(Stages::Second, systems::serialize_and_send_full)
            .add_system_to_stage(Stages::Second, systems::emit_changed::<IWDTransform>)
            .add_system_to_stage(Stages::Second, systems::send_delta)
            .set_runner(Self::engine_loop)
            .run();
    }

    fn engine_loop(mut app: App) {
        info!("Starting game loop");
        loop {
            let t = Instant::now();
            app.update();
            std::thread::sleep(GAME_TICK - t.elapsed())
        }
    }
}

#[derive(StageLabel)]
pub enum Stages {
    Global,
    First,
    Second,
}
