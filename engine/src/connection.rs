use bevy::prelude::{trace, Component};
use crossbeam_channel::{Sender, TrySendError};

use state::*;

use crate::message::Response;

pub type ValidConnection = Connection<Connected>;

#[derive(Debug, Component, Clone, Copy, PartialEq, Eq)]
pub struct Connection<State> {
    state: State,
}

impl Default for Connection<NotConnected> {
    fn default() -> Self {
        Self {
            state: NotConnected,
        }
    }
}
impl Connection<Connected> {
    pub fn new(sender: ClientSender) -> Self {
        Self {
            state: Connected(sender),
        }
    }
    pub fn send(&self, message: Response) {
        if let Err(e) = self.state.0.try_send(message) {
            match e {
                TrySendError::Disconnected(m) => {
                    trace!("Could not sent client response: {m:?} as the channel is Disconnected. Despawn should happen");
                }
                other => panic!("{}", other),
            }
        }
    }
}

pub type ClientSender = Sender<Response>;

pub mod state {

    use bevy::prelude::Component;

    use super::ClientSender;
    #[derive(Debug, Component, Clone)]
    pub struct Connected(pub(super) ClientSender);

    #[derive(Debug, Component, Clone, Copy, PartialEq, Eq)]
    pub struct NotConnected;
}
