use crate::{
    connection::{state::NotConnected, Connection},
    Aggro, IWDTransform, Name, Stats,
};
use bevy::prelude::*;
use serde::Serialize;

#[derive(Debug, Component, Clone)]
pub struct Player {
    pub username: String,
    pub characters: Vec<Name>,
}

#[derive(Debug, Bundle)]
pub struct PlayerBundle<ConnState: Send + Sync + 'static> {
    pub aggro: Aggro,
    pub stats: Stats,
    pub transform: IWDTransform,
    pub player: Player,
    pub name: Name,
    pub connection: Connection<ConnState>,
    pub id: ID,
}

#[derive(Debug, Component, Clone, Copy, Serialize, Default, Reflect)]
#[reflect_value(Component, Serialize)]
pub struct ID(pub u64);

impl Default for PlayerBundle<NotConnected> {
    fn default() -> Self {
        Self {
            aggro: Aggro::default(),
            stats: Stats::default(),
            transform: IWDTransform::default(),
            player: Player {
                username: String::new(),
                characters: Vec::new(),
            },
            name: Name::default(),
            connection: Connection::default(),
            id: ID::default(),
        }
    }
}
