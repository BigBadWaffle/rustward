use crate::{
    connection::ValidConnection,
    events::{Delta, FullSyncRequired},
    message::Response,
    Aggro, Stats,
};
use bevy::scene::serde::SceneSerializer;
use std::sync::Arc;

use bevy::prelude::*;

pub mod actions;
pub mod character;

pub fn regen_hp(mut query: Query<(&mut Stats, &Aggro)>) {
    for (mut stats, aggro) in query.iter_mut() {
        if aggro.is_in_combat || stats.hp == stats.hp_max {
            continue;
        }
        stats.hp += 1;
    }
}

pub fn emit_changed<T: Component>(
    deltas: Query<(Entity, Changed<T>)>,
    mut writer: EventWriter<Delta>,
) {
    for ent in deltas
        .iter()
        .filter(|(_, changed)| *changed)
        .map(|(ent, _)| ent)
    {
        writer.send(Delta(ent));
    }
}

pub fn send_delta(
    world: &World,
    registry: Res<AppTypeRegistry>,
    connections: Query<&ValidConnection>,
    mut reader: EventReader<Delta>,
) {
    for Delta(ent) in reader.iter() {
        let mut builder = DynamicSceneBuilder::from_world(world);
        builder.extract_entity(*ent);
        let scene = builder.build();
        let ser = SceneSerializer::new(&scene, &registry);
        let val = Arc::new(serde_json::to_value(&ser).unwrap());
        for connection in connections.iter() {
            let val = Arc::clone(&val);
            connection.send(Response::Delta(val));
        }
    }
}

pub fn serialize_and_send_full(
    world: &World,
    registry: Res<AppTypeRegistry>,
    mut required: EventReader<FullSyncRequired>,
    connections: Query<(Entity, &ValidConnection)>,
) {
    // Only send full sync when someone is added.
    for _ in required.iter() {
        if world.entities().is_empty() {
            return;
        }
        let scene = DynamicScene::from_world(world, &registry);

        let ser = SceneSerializer::new(&scene, &registry);
        let val = Arc::new(serde_json::to_value(&ser).unwrap());
        for (ent, connection) in connections.iter() {
            let val = Arc::clone(&val);
            trace!("Sending sync from to player {ent:?}");
            connection.send(Response::SyncAll(val));
        }
    }
}
