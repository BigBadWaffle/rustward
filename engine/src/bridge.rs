use bevy::prelude::*;
use crossbeam_channel::Receiver;

use crate::{actions::NetworkAction, auth::Login, inventory::Inventory, message::Response};

use self::state::{Authenticated, EngineLinkCreated};

// pub trait Bridge: Network + Database {}
pub trait Network: Send + Sync + 'static + Clone + Resource + Copy {
    fn register_entity_on_connection(
        &self,
        entity_id: Entity,
        connection_id: u64,
        connection_receiver: Receiver<Response>,
    );
    fn try_receive_login(&self) -> Option<Login>;
    fn try_receive_action(&self) -> Option<NetworkAction>;
}
pub struct Auth<State> {
    state: State,
}
impl<Any> Auth<Any> {
    pub fn authenticate<I>(user_id: I) -> Auth<Authenticated<I>> {
        Auth {
            state: Authenticated { user_id },
        }
    }
}
impl<I> Auth<Authenticated<I>> {
    pub fn link_player_to_database(self, player_id: Entity) -> Auth<EngineLinkCreated<I>> {
        let Auth { state } = self;
        let Authenticated { user_id } = state;
        Auth {
            state: EngineLinkCreated { user_id, player_id },
        }
    }
}
impl<I> Auth<EngineLinkCreated<I>> {
    pub fn player_id(&self) -> &Entity {
        &self.state.player_id
    }
    pub fn user_id(&self) -> &I {
        &self.state.user_id
    }
}
pub mod state {
    use bevy::prelude::Entity;

    pub struct Authenticated<I> {
        pub(super) user_id: I,
    }
    pub struct EngineLinkCreated<I> {
        pub(super) player_id: Entity,
        pub(super) user_id: I,
    }
}
pub trait Database: Send + Sync + 'static + Clone + Resource + Copy {
    type Error;
    type UserId;
    fn authenticate(
        &self,
        username: &str,
        password: &str,
    ) -> Result<Auth<Authenticated<Self::UserId>>, Self::Error>;
    fn link_player_to_database(
        &self,
        auth: Auth<Authenticated<Self::UserId>>,
        player_id: Entity,
    ) -> Result<Auth<EngineLinkCreated<Self::UserId>>, Self::Error>;
    fn get_character_list(&self, user: Auth<EngineLinkCreated<Self::UserId>>) -> Vec<Name>;
    fn get_character_inventory(&self, character_name: &str) -> Result<Inventory, Self::Error>;
}
