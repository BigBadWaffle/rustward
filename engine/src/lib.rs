use crate::player::{Player, PlayerBundle};

use std::{
    ops::{Add, AddAssign},
    time::Duration,
};

use bevy::prelude::*;

use connection::Connection;

use player::ID;
use serde::Deserialize;
use serde::Serialize;

pub const GAME_TICK: Duration = Duration::from_millis(350);

pub mod actions;
pub mod auth;
pub(crate) mod character;
pub(crate) mod connection;
pub mod inventory;
pub mod message;
pub mod output;
pub(crate) mod plugins;
// pub(crate) mod scene;
pub(crate) mod commands;
pub(crate) mod schedule;
pub(crate) mod systems;
pub use schedule::Engine;
pub mod bridge;
pub(crate) mod events;
pub(crate) mod player;

#[derive(Debug, Serialize, Clone)]
pub enum Faction {
    Players,
    Hostile,
}

#[derive(Debug, Component, Serialize, Clone)]
pub struct Stats {
    hp: usize,
    hp_max: usize,
}

impl Default for Stats {
    fn default() -> Self {
        Self {
            hp: 100,
            hp_max: 100,
        }
    }
}

#[derive(Debug, Component, Serialize, Clone)]
pub struct Aggro {
    faction: Faction,
    is_in_combat: bool,
}

impl Default for Aggro {
    fn default() -> Self {
        Self {
            faction: Faction::Players,
            is_in_combat: false,
        }
    }
}

#[derive(Debug, Component, Serialize, Clone)]
pub struct Npc;

#[derive(Debug, Bundle)]
pub struct NpcBundle {
    pub aggro: Aggro,
    pub stats: Stats,
    pub npc: Npc,
    pub transform: IWDTransform,
    pub name: Name,
}

impl Default for NpcBundle {
    fn default() -> Self {
        Self {
            aggro: Aggro::default(),
            stats: Stats::default(),
            transform: IWDTransform::default(),
            npc: Npc,
            name: Name::default(),
        }
    }
}

#[derive(Debug, Component, Clone, Copy, Serialize, PartialEq, Eq, Reflect)]
#[reflect(Component)]
pub struct IWDTransform {
    position: IVec2,
}

impl From<IVec2> for IWDTransform {
    fn from(v: IVec2) -> Self {
        Self { position: v }
    }
}

impl Default for IWDTransform {
    fn default() -> Self {
        IWDTransform {
            position: IVec2::new(0, 0),
        }
    }
}

#[derive(Debug, Component, Deserialize, Serialize, Clone, Copy, PartialEq, Eq)]
pub struct Vector(pub usize, pub usize);

impl Add for Vector {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        Self(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl AddAssign for Vector {
    fn add_assign(&mut self, rhs: Self) {
        self.0 += rhs.0;
        self.1 += rhs.1;
    }
}

impl From<(usize, usize)> for Vector {
    fn from(d: (usize, usize)) -> Self {
        Self(d.0, d.1)
    }
}

// #[derive(Debug, Component, Clone, Copy, Serialize, Default, Reflect)]
// #[reflect_value(Component, Serialize)]
// #[serde(transparent)]
// pub struct Delta<T: Component + Serialize + Reflect + Clone + Default>(T);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vec_add() {
        let v = Vector(1, 1);
        let v2 = Vector(1, 1);
        assert_eq!(v + v2, Vector(2, 2));
    }
}
