use bevy::prelude::{Entity, IVec2};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, Deserialize, Serialize, PartialEq, Eq)]
pub struct Move(IVec2);

impl Move {
    pub fn delta(&self) -> IVec2 {
        self.0
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub struct Respawn;

/// Actions from clients over socket
#[derive(Debug, Clone, Copy)]
pub struct Action<Data> {
    /// Who performed the action
    /// The server adds this for us based on the connection to the client and the spawned player.
    /// If spawn did not happen a client cannot perform an action. Login is special in this case
    pub entity_id: Entity,
    pub data: Data,
}
/// Actions from clients over socket
#[derive(Debug, Clone)]
pub struct NetworkAction {
    /// Who performed the action
    /// The server adds this for us based on the connection to the client and the spawned player.
    /// If spawn did not happen a client cannot perform an action. Login is special in this case
    pub entity_id: Entity,
    pub data: ActionData,
}

impl<AnyData> PartialEq<Action<AnyData>> for Entity {
    fn eq(&self, other: &Action<AnyData>) -> bool {
        self == &other.entity_id
    }
}

impl<AllData> Action<AllData> {
    pub fn new(entity_id: Entity, data: AllData) -> Self {
        Self { entity_id, data }
    }
}

//TODO: We should write a derive macro for this enum
//      when we add an action here the following codegen should happen
//      * Create the `ActionDataType` enum
//      * Implement the `PartialEq` between the 2 enums
//      * Generate the systems that write to events
//          * These systems should be under a plugin that will always register them all in the engine
#[derive(Debug, Deserialize, Clone, PartialEq, Eq)]
pub enum ActionData {
    Move(Move),
    Respawn(Respawn),
    SpawnCharacter(SpawnCharacter),
    Despawn,
}
#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Eq)]
pub enum ActionDataType {
    Move,
    Respawn,
    Despawn,
    SpawnCharacter,
}
impl PartialEq<ActionDataType> for ActionData {
    fn eq(&self, other: &ActionDataType) -> bool {
        match self {
            ActionData::Move(_) => matches!(other, ActionDataType::Move),
            ActionData::Respawn(_) => matches!(other, ActionDataType::Respawn),
            ActionData::Despawn => matches!(other, ActionDataType::Despawn),
            ActionData::SpawnCharacter(_) => matches!(other, ActionDataType::SpawnCharacter),
        }
    }
}
pub(crate) mod state {
    #[derive(Debug)]
    pub struct Unchecked;
    /// A state where we applied a check to make sure that the action type matches the data
    #[derive(Debug)]
    pub struct Verified;
}

#[derive(Debug, Deserialize, Clone, PartialEq, Eq)]
pub struct SpawnCharacter(pub String);
