use serde::{Deserialize, Serialize};

#[derive(Debug, Clone)]
pub struct Login {
    pub data: LoginData,
    pub server_connection_id: u64,
}
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct LoginData {
    pub username: String,
    pub password: String,
}

impl Login {
    // pub fn new(
    //     username: String,
    //     password: String,
    //     connection_id: u64,
    //     sender: ClientSender<Box<dyn ClientResponse>>,
    //     receiver: Box<dyn ClientAction>,
    // ) -> Self {
    //     Self {
    //         username,
    //         password,
    //         sender,
    //         receiver,
    //     }
    // }
}
#[derive(Debug, Clone, Serialize)]
pub enum LoginResult {
    Success { characters: Vec<String> },
    Failure,
}
