use bevy::{prelude::*, utils::HashSet};
use serde::Serialize;

use crate::inventory::Inventory;

#[derive(Debug, Bundle, Default)]
pub struct CharacterBundle {
    character: Character,
    inventory: Inventory,
    equipment: Equipement,
    name: Name,
}
#[derive(Debug, Component, Clone, Serialize, Default, Reflect)]
#[reflect_value(Component, Serialize)]
pub struct Character {
    pub in_view: HashSet<Entity>,
}

#[derive(Debug, Component, Clone, Copy, Serialize, Default, Reflect)]
#[reflect_value(Component, Serialize)]
pub struct Equipement;
