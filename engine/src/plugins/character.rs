use crate::{
    bridge::Database,
    character::{Character, Equipement},
    commands,
    inventory::{Damage, Inventory, InventoryItem, Spell, SpellType, Sprite},
    systems,
};

use bevy::prelude::*;

use crate::{bridge::Network, schedule::Stages};

pub struct CharacterPlugin<Network, Db> {
    _empty: Network,
    _empty2: Db,
}
impl<N: Network, D: Database> CharacterPlugin<N, D> {
    pub fn new(network_bridge: N, database_bridge: D) -> Self {
        Self {
            _empty: network_bridge,
            _empty2: database_bridge,
        }
    }
}
impl<N: Network, D: Database> Plugin for CharacterPlugin<N, D> {
    fn build(&self, app: &mut App) {
        app.register_type::<Character>()
            .register_type::<Inventory>()
            .register_type::<Equipement>()
            .register_type::<InventoryItem>()
            .register_type::<Spell>()
            .register_type::<SpellType>()
            .register_type::<Sprite>()
            .register_type::<Damage>()
            .add_system_to_stage(
                Stages::Global,
                systems::character::remove_despawned_from_seen,
            )
            .add_system_to_stage(Stages::First, systems::character::move_player)
            .add_system_to_stage(Stages::Second, systems::character::add_unseen_entities)
            .add_system_to_stage(Stages::Second, systems::character::update_in_view)
            .add_system_to_stage(Stages::Second, systems::character::require_full_sync_on_add)
            .add_system_to_stage(
                Stages::Second,
                systems::character::require_full_sync_on_remove,
            )
            .add_system_to_stage(
                Stages::Second,
                commands::character::spawn_character_for_player::<D>,
            );
    }
    fn name(&self) -> &str {
        "Character plugin"
    }
}
