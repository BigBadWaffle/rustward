use crate::ID;
use crate::{bridge::Database, commands};
use bevy::prelude::*;

use crate::{bridge::Network, schedule::Stages};

pub struct PlayerPlugin<Network, Db> {
    _empty: Network,
    _empty2: Db,
}
impl<N: Network, D: Database> PlayerPlugin<N, D> {
    pub fn new(network_bridge: N, database_bridge: D) -> Self {
        Self {
            _empty: network_bridge,
            _empty2: database_bridge,
        }
    }
}
impl<N: Network, D: Database> Plugin for PlayerPlugin<N, D> {
    fn build(&self, app: &mut App) {
        app.register_type::<ID>()
            .add_system_to_stage(Stages::Global, commands::player::spawn::<N, D>)
            .add_system_to_stage(Stages::Global, commands::player::despawn_disonnected);
    }
    fn name(&self) -> &str {
        "Player plugin"
    }
}
