use bevy::prelude::*;

use crate::bridge::Network;
use crate::inventory::*;

pub struct InventoryPlugin<NetworkBridge> {
    _empty: NetworkBridge,
}
impl<B: Network> InventoryPlugin<B> {
    pub fn new(bridge: B) -> Self {
        Self { _empty: bridge }
    }
}
impl<B: Network> Plugin for InventoryPlugin<B> {
    fn build(&self, app: &mut App) {
        app.register_type::<Inventory>()
            .register_type::<InventoryItem>()
            .register_type::<InventoryLevel>()
            .register_type::<SpellType>()
            .register_type::<Spell>()
            .register_type::<Sprite>()
            .register_type::<Damage>();
        // .add_system_to_stage(Stages::Second, i)
    }
    fn name(&self) -> &str {
        "Inventory Plugin"
    }
}
