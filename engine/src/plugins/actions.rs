use bevy::prelude::*;

use crate::actions::SpawnCharacter;
use crate::{bridge::Network, schedule::Stages};

use crate::{
    actions::{Action, Move, NetworkAction, Respawn},
    systems,
};

pub struct AllActions<NetworkBridge> {
    _empty: NetworkBridge,
}
impl<B: Network> AllActions<B> {
    pub fn new(bridge: B) -> Self {
        Self { _empty: bridge }
    }
}
impl<B: Network> Plugin for AllActions<B> {
    fn build(&self, app: &mut App) {
        app.add_event::<Action<Respawn>>()
            .add_event::<Action<Move>>()
            .add_event::<Action<SpawnCharacter>>()
            .add_event::<NetworkAction>()
            .add_system_to_stage(Stages::Global, systems::actions::write_client_login::<B>)
            .add_system_to_stage(Stages::Global, systems::actions::read_unused_move)
            .add_system_to_stage(Stages::Global, systems::actions::read_unused_respawn)
            .add_system_to_stage(
                Stages::Global,
                systems::actions::read_unused_spawn_character,
            )
            .add_system_to_stage(Stages::Global, systems::actions::write_client_respawn::<B>)
            .add_system_to_stage(
                Stages::Global,
                systems::actions::write_client_spawn_character::<B>,
            )
            .add_system_to_stage(Stages::Global, systems::actions::write_client_move::<B>);
    }
    fn name(&self) -> &str {
        "Action plugin"
    }
}
