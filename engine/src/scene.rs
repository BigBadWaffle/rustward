use bevy::ecs::reflect::ReflectComponent;
use bevy::reflect::ReflectSerialize;
use serde::Serialize;
use std::ops::Deref;
use std::ops::DerefMut;

use bevy::prelude::Bundle;
use bevy::prelude::Component;
use bevy::prelude::World;
use bevy::reflect::Reflect;

use crate::IWDTransform;
use crate::Name;

#[derive(Debug, Bundle, Default)]
pub struct PublicFull {
    pub transform: IWDTransform,
    pub name: Name,
    pub id: ID,
}

#[derive(Debug, Component, Clone, Copy, Serialize, Default, Reflect)]
#[reflect_value(Component, Serialize)]
pub struct ID(pub u64);

#[derive(Debug, Default)]
pub struct PublicFullWorld(World);

impl Deref for PublicFullWorld {
    type Target = World;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for PublicFullWorld {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
