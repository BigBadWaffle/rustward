use std::sync::Mutex;

use bevy::{prelude::*, utils::HashMap};
use engine::{
    bridge::{self, state::Authenticated, Auth},
    inventory::Inventory,
};
use thiserror::Error;

#[derive(Debug, Copy, Clone, Resource)]
pub struct Database {
    players: &'static Mutex<HashMap<Entity, String>>,
}

impl Database {
    pub fn new() -> Self {
        Self {
            players: Box::leak(Box::new(Mutex::new(HashMap::new()))),
        }
    }
}

#[derive(Debug, Error)]
pub enum DatabaseError {}

impl bridge::Database for Database {
    type Error = DatabaseError;
    type UserId = String;

    fn authenticate(
        &self,
        username: &str,
        password: &str,
    ) -> Result<bridge::Auth<bridge::state::Authenticated<Self::UserId>>, Self::Error> {
        Ok(Auth::<Self::UserId>::authenticate(username.to_string()))
    }

    fn get_character_list(
        &self,
        user: Auth<bridge::state::EngineLinkCreated<Self::UserId>>,
    ) -> Vec<Name> {
        vec![Name::new("test-char")]
    }

    fn get_character_inventory(&self, character_name: &str) -> Result<Inventory, Self::Error> {
        Ok(Inventory::default())
    }

    fn link_player_to_database(
        &self,
        auth: Auth<Authenticated<Self::UserId>>,
        player_id: Entity,
    ) -> Result<Auth<bridge::state::EngineLinkCreated<Self::UserId>>, Self::Error> {
        let link = auth.link_player_to_database(player_id);
        self.players
            .lock()
            .unwrap()
            .insert(player_id, link.user_id().to_string());
        Ok(link)
    }
}
